<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CalendarRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *   normalizationContext={"groups"={"read:calendar"}},
 *   collectionOperations={"get", "post"={"normalization_context"={"groups"="post:calendar"}}},
 *   itemOperations={"get","delete","put"}
 * )
 * @ORM\Entity(repositoryClass=CalendarRepository::class)
 */
class Calendar
{
    /**
     * @Groups({"read:calendar"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read:calendar", "post:calendar"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Groups({"read:calendar", "post:calendar"})
     * @ORM\Column(type="datetime")
     */
    private $start_date;

    /**
     * @Groups({"read:calendar", "post:calendar"})
     * @ORM\Column(type="datetime")
     */
    private $end_date;

    /**
     * @Groups({"read:calendar", "post:calendar"})
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Groups({"read:calendar", "post:calendar"})
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $all_day;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->start_date;
    }

    public function setStartDate(\DateTimeInterface $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->end_date;
    }

    public function setEndDate(\DateTimeInterface $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAllDay(): ?bool
    {
        return $this->all_day;
    }

    public function setAllDay(?bool $all_day): self
    {
        $this->all_day = $all_day;

        return $this;
    }
}
