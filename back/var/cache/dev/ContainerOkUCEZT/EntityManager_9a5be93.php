<?php

namespace ContainerOkUCEZT;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder4e279 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerb83f9 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties80c36 = [
        
    ];

    public function getConnection()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getConnection', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getMetadataFactory', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getExpressionBuilder', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'beginTransaction', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getCache', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getCache();
    }

    public function transactional($func)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'transactional', array('func' => $func), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->transactional($func);
    }

    public function commit()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'commit', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->commit();
    }

    public function rollback()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'rollback', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getClassMetadata', array('className' => $className), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'createQuery', array('dql' => $dql), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'createNamedQuery', array('name' => $name), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'createQueryBuilder', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'flush', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'clear', array('entityName' => $entityName), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->clear($entityName);
    }

    public function close()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'close', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->close();
    }

    public function persist($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'persist', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'remove', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'refresh', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'detach', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'merge', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getRepository', array('entityName' => $entityName), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'contains', array('entity' => $entity), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getEventManager', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getConfiguration', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'isOpen', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getUnitOfWork', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getProxyFactory', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'initializeObject', array('obj' => $obj), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'getFilters', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'isFiltersStateClean', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'hasFilters', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return $this->valueHolder4e279->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerb83f9 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder4e279) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder4e279 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder4e279->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__get', ['name' => $name], $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        if (isset(self::$publicProperties80c36[$name])) {
            return $this->valueHolder4e279->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e279;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder4e279;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e279;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder4e279;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__isset', array('name' => $name), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e279;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder4e279;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__unset', array('name' => $name), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder4e279;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder4e279;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__clone', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        $this->valueHolder4e279 = clone $this->valueHolder4e279;
    }

    public function __sleep()
    {
        $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, '__sleep', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;

        return array('valueHolder4e279');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerb83f9 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerb83f9;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerb83f9 && ($this->initializerb83f9->__invoke($valueHolder4e279, $this, 'initializeProxy', array(), $this->initializerb83f9) || 1) && $this->valueHolder4e279 = $valueHolder4e279;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder4e279;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder4e279;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
