<?php

namespace App\Controller;

use DateTime;
use App\Entity\Calendar;
use App\Form\CalendarType;
use App\Service\CallApiCalendar;
use App\Service\Notificationparmail;
use App\Repository\CalendarRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/calendar')]
class CalendarController extends AbstractController
{
    #[Route('/', name: 'calendar_index', methods: ['GET'])]
    public function index(CallApiCalendar $callApiCalendar): Response
    {
        $calendars = $callApiCalendar->getRdvData();
        
        return $this->render('calendar/index.html.twig', [
            'calendars' => $calendars
        ]);
    }

    #[Route('/new', name: 'calendar_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CallApiCalendar $callApiCalendar, Notificationparmail $notificationParMail): Response
    {
        
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        
        $calendar = new Calendar();
        $form = $this->createForm(CalendarType::class, $calendar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $a = $serializer->serialize($calendar,'json');
            $startDate = $calendar->getStartDate();
            $endDate   = $calendar->getEndDate();
            $arr = json_decode($a);
            $arr->startDate = $startDate->format('Y-m-d H:i:s');
            $arr->endDate = $endDate->format('Y-m-d H:i:s');
            $arr = json_encode($arr);
            $callApiCalendar->postRdvData($arr);
            //$notificationParMail->sendEmail($calendar['title']);

            return $this->redirectToRoute('calendar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('calendar/new.html.twig', [
            'calendar' => $calendar,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'calendar_show', methods: ['GET'])]
    public function show(int $id, CallApiCalendar $callApiCalendar): Response
    {
        return $this->render('calendar/show.html.twig', [
            'calendar' => $callApiCalendar->getRdvDataById($id),
        ]);
    }

    #[Route('/{id}/edit', name: 'calendar_edit', methods: ['GET', 'POST'])]
    public function edit(int $id,Request $request, CallApiCalendar $callApiCalendar,Notificationparmail $notificationParMail): Response
    {
        
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $calendars = $callApiCalendar->getRdvDataById($id);
        $date = new \DateTime();
        $calendars['start_date'] = $date->setTimestamp(strtotime($calendars['start_date']));
        $calendars['end_date'] = $date->setTimestamp(strtotime($calendars['end_date']));
        $calendarObj = new Calendar();
        $calendarObj->setTitle($calendars['title']);
        $calendarObj->setStartDate($calendars['start_date']);
        $calendarObj->setEndDate($calendars['end_date']);
        $calendarObj->setDescription($calendars['description']);
        $calendarObj->setAllDay($calendars['all_day']);

        $form = $this->createForm(CalendarType::class, $calendarObj);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calendar = $request->request->all('calendar');
            unset($calendar['_token']);
            $calendar['start_date'] = $calendar['start_date']['date']['month'].'-'.$calendar['start_date']['date']['day'].'-'.$calendar['start_date']['date']['year'];
            $calendar['end_date'] = $calendar['end_date']['date']['month'].'-'.$calendar['end_date']['date']['day'].'-'.$calendar['end_date']['date']['year'];
            $callApiCalendar->putRdvData(json_encode($calendar),$id);
            //$notificationParMail->sendEmail($calendar['title']);
            return $this->redirectToRoute('calendar_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('calendar/edit.html.twig', [
            'calendar' => $calendars,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'calendar_delete', methods: ['POST'])]
    public function delete(int $id, Request $request, CallApiCalendar $callApiCalendar): Response
    {
        if ($this->isCsrfTokenValid('delete'.$id, $request->request->get('_token'))) {
            $callApiCalendar->deleteRdvById($id);
        }

        return $this->redirectToRoute('calendar_index', [], Response::HTTP_SEE_OTHER);
    }
}
