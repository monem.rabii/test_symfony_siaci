<?php
 
namespace App\Service;

use App\Entity\Calendar;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Contracts\HttpClient\HttpClientInterface;
 
class CallApiCalendar
{
    private $client;
 
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }
 
    public function getRdvData(): array
    {
        $response = $this->client->request(
            'GET',
            'http://127.0.0.1:8001/api/calendars?page=1'
        );
 
        return $response->toArray();
    }

    public function getRdvDataById($id): array
    {
        $response = $this->client->request(
            'GET',
            'http://127.0.0.1:8001/api/calendars/'.$id
        );
 
        return $response->toArray();
    }

    public function deleteRdvById($id)
    {
        $response = $this->client->request(
            'DELETE',
            'http://127.0.0.1:8001/api/calendars/'.$id
        ); 
    }


    public function putRdvData(String $calendar,int $id)
    {
        $response = $this->client->request(
            'PUT',
            'http://127.0.0.1:8001/api/calendars/'.$id,
            [   
                'headers' => [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ],
                'body' => $calendar
            ]
        );
     
    }

    

    public function postRdvData(String $calendar)
    {
        $response = $this->client->request(
            'POST',
            'http://127.0.0.1:8001/api/calendars',
            [   
                'headers' => [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ],
                'body' => $calendar
            ]
        );
    }

}