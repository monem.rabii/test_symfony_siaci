<?php

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;
use App\Repository\IPRepository;
use Doctrine\ORM\EntityManagerInterface;

class Notificationparmail
{
    protected $mailer;
    private $em;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $em)
    {
        $this->mailer = $mailer;
        $this->em = $em;
    }

    public function sendEmail(String $title)
    {
        $from = 'client@serveur.fr';
        $objet = "Enregistrement d'un nouveau RDV " . $title;
        $this->em->flush();
        $message = (new \Swift_Message())
            ->setSubject($objet)
            ->setFrom($from)
            ->setTo('admin@siaci.fr')
            ->setBody(
                "Notification d'un changement de RDV",
                'text/html'
            );
        $this->mailer->send($message);
    }
}
