<?php

namespace ContainerUtlxjnV;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder6d902 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer26b57 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties88ec1 = [
        
    ];

    public function getConnection()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getConnection', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getMetadataFactory', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getExpressionBuilder', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'beginTransaction', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getCache', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getCache();
    }

    public function transactional($func)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'transactional', array('func' => $func), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->transactional($func);
    }

    public function commit()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'commit', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->commit();
    }

    public function rollback()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'rollback', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getClassMetadata', array('className' => $className), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'createQuery', array('dql' => $dql), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'createNamedQuery', array('name' => $name), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'createQueryBuilder', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'flush', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'clear', array('entityName' => $entityName), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->clear($entityName);
    }

    public function close()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'close', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->close();
    }

    public function persist($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'persist', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'remove', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'refresh', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'detach', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'merge', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getRepository', array('entityName' => $entityName), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'contains', array('entity' => $entity), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getEventManager', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getConfiguration', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'isOpen', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getUnitOfWork', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getProxyFactory', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'initializeObject', array('obj' => $obj), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'getFilters', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'isFiltersStateClean', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'hasFilters', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return $this->valueHolder6d902->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer26b57 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder6d902) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder6d902 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder6d902->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__get', ['name' => $name], $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        if (isset(self::$publicProperties88ec1[$name])) {
            return $this->valueHolder6d902->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6d902;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder6d902;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6d902;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder6d902;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__isset', array('name' => $name), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6d902;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder6d902;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__unset', array('name' => $name), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6d902;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder6d902;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__clone', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        $this->valueHolder6d902 = clone $this->valueHolder6d902;
    }

    public function __sleep()
    {
        $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, '__sleep', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;

        return array('valueHolder6d902');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer26b57 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer26b57;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer26b57 && ($this->initializer26b57->__invoke($valueHolder6d902, $this, 'initializeProxy', array(), $this->initializer26b57) || 1) && $this->valueHolder6d902 = $valueHolder6d902;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6d902;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6d902;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
