<?php

namespace ContainerYNSYxro;


use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_CZEZ9anService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.cZEZ9an' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.cZEZ9an'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'callApiCalendar' => ['privates', 'App\\Service\\CallApiCalendar', 'getCallApiCalendarService', true],
        ], [
            'callApiCalendar' => 'App\\Service\\CallApiCalendar',
        ]);
    }
}
